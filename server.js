const express = require('express');
const http = require('http');
const path = require('path');

const app = express();

const port = process.env.PORT || '8080';
app.set('port', port);

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/hello-world/index.html'));
});

const server = http.createServer(app);
server.listen(port, () => console.log('Running'));
