#Learning Progress 2 (July 30 2018)

---

To make our page accept input from the user (Usually, it's about creating a form input)

1. Importing `input` module
1. `@Input` annotation describe that the template accepting input from the parent template

Step by step how Angular project being compiled:

1. Use command `ng serve`
1. Finding `angular.json` file. In this file, main entry point specified at `main.ts`
1. Use `AppModule` that specified in `src/app/app.module.ts` to bootstrap the app.
1. `AppModule` specify which component to use as the top-level component (In this case, `AppComponent`)
1. `AppComponent` has `<app-user-list>` tags in the template and this renders our list of users.

In bootstraping the app, Angular only fired up the component that specified at `NgModule`

Suppose we have this code:

```angularjs
@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    UserItemComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
```

There are 4 keys that important at `NgModule` declaration:

1. `declaration` : Defining components that can be used in this module. Always
declare each component that want to be used in this keys before used
1. `imports` : Describing which dependencies this module has. For example,
because we want to create browser app, so we import `BrowserModule`
1. `providers` : Used for dependency injection
1. `bootstrap` : Describing when this module is used to bootstrap an app.

When binding input to value, lets use this example:

```angular2html
<input name="title" #newtitle>
```

The `#newtitle` is called resolve, which bind the `<input>` to the variable that been assigned.

The `(click)` describing the event listener when the HTML element is clicked.

JavaScript, by default, propagates *the click event to all the parent components*. Because the
click event is propagated to parents, our browser is trying to follow the empty link, which tells the
browser to reload.

A good practice when writing Angular code is to try to isolate the data structures we are using from
the component code. We can treat this as creating a *Model* in MVC pattern. When we want to define optional attribute, try using `?` character in 
attribute name.

##How to deploy the application (Heroku)

1. Create file `Procfile` that contains following code
```text
web: node server.js
```
1. Install Express,js
```text
npm install --save express
```
1. Create file `server.js` at root of your project (1 level with package.json), containing these codes (Suppose that your project name is `hello-world`):
```javascript
const express = require('express');
const http = require('http');
const path = require('path');

const app = express();

const port = process.env.PORT || '8080';
app.set('port', port);

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/hello-world/index.html'));
});

const server = http.createServer(app);
server.listen(port, () => console.log('Running'));
```
1. Try running `server.js` using this command:
```text
node server.js
```
1. Make sure you achieve several things
  - [ ] Define `main` key with the value is `server.js` in the `package.json`
  - [ ] Define `engines` key with the value in the `package.json` as defined:
    ```javascript
    "engines" : {"node" : NODE_VERSION, "npm": NPM_VERSION}
    ```
    Fill the value of **NODE_VERSION** and **NPM_VERSION** as used in your machine
  - [ ] Move all `devDependencies` to `dependencies` in the `package.json`
  - [ ] Insert new Key to `scripts` key in the `package.json` named `postinstall`, with this following value:
    ```javascript
    "postinstall" : "ng build --base-href /hello-world/ --aot --prod --output-hashing none"
    ```
    Assuming your project name is `hello-world`
1. Create your heroku apps and push it
