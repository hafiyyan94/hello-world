#Learning Progress 1 (July 19 2018)

---

Angular Component : Teaching HTML a new customizable tag
Creating component can be done by using this command:

```bash
ng generate component <your-component>
```

The code `OnInit run code when we initialize the component`

Th syntax below is called destructuring, that provided by ES6 and Typescript:

```angularjs
import {something} from wherever
```

The snippet code below, `@Component` is called decorators:

```angularjs
@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
```

Imagine this as metadata added to our code. The next 3 line is described below:

1. selector : Defining which html tag in `index.html` that will be used to render this component
1. templateUrl : Defining the path of template that will be used for this component.
template also can be defined using inline template. Just change the tag `templateUrl` into `template` tag.
1. styleUrls Defining the path of styling file that will be used for this component.

Please note that to define multiline string (String value that will take multiple line to defined),
we can use backstick character (Shift+ <button in the left of button 1>)

We can also define a property inside the class. Suppose we want to have property 
named `name` with the type is `string`. Here is what it looks like:

```angularjs
export class UserItemComponent implements OnInit {

  name: string;

  constructor() {
    this.name = 'Felipe';
  }

  ngOnInit() {
  }

}
```

Suppose that we have Array property in our class, and we want to display every item in that list by iterating. we can `*ngFor` syntax to do iterating in template. Here
is the code sample in the html:

```angularjs
<ul>
  <li *ngFor="let name of names">Hello {{ name }}</li>
</ul>
```
`"let name of names"` is the declaration of iteration logic. `let name` is defining the reference.
